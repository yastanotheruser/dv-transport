const HTMLPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: './src/js/main.js',
    output: {
        path: __dirname + '/dist/',
        filename: 'dv.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/proposal-class-properties']
                    }
                }
            },

            {
                test: /\.css$/,
                use: [MiniCSSExtractPlugin.loader, "css-loader"]
            }
        ]
    },

    plugins: [
        new HTMLPlugin(),
        new MiniCSSExtractPlugin({ filename: 'style.css' }),
        new OptimizeCSSAssetsPlugin({ assetNameRegExp: /\.css$/ })
    ]
};