const adjMatrixScope = new WeakMap(), weightScope = new WeakMap();
export class Graph {
    get adjMatrix() {
        return adjMatrixScope.get(this);
    }

    set adjMatrix(matrix) {
        if (!isValidAdjacencyMatrix(matrix))
            throw new TypeError("Invalid adjacency matrix");

        adjMatrixScope.set(this, matrix);
    }

    get weightMatrix() {
        return weightScope.get(this);
    }

    set weightMatrix(matrix) {
        if (matrix == null)
            return this.clearWeights();

        const adjMatrix = this.adjMatrix, size = adjMatrix.length;
        if (!isValidWeightMatrix(matrix, size))
            throw new TypeError("Invalid weight matrix");

        for (let i = 0; i < size; i++) {
            for (let j = 0; j < size; j++) {
                if (!adjMatrix[i][j])
                    matrix[i][j] = null;
            }
        }

        weightScope.set(this, matrix);
    }

    constructor(matrix, weights) {
        if (matrix == null)
            matrix = [];

        this.adjMatrix = matrix;
        this.weightMatrix = weights;
    }

    insertNode(edges, weights) {
        if (edges == null)
            edges = [];

        if (weights == null)
            weights = {};

        if (!Object.prototype.isPrototypeOf(edges))
            throw new TypeError(`Invalid initial edges, ${edges} is not an object`);

        if (!Object.prototype.isPrototypeOf(weights))
            throw new TypeError(`Invalid initial weights, ${weights} is not an object`);

        const adjMatrix = this.adjMatrix;
        for (let i = 0; i < adjMatrix.length; i++) {
            const r = adjMatrix[i];
            if (edges[i] == 2) {
                r.push(1);
                edges[i]--;
                continue;
            }

            r.push(0);
        }

        const lastIndex = adjMatrix.length;
        const r = new Array(lastIndex + 1);
        adjMatrix.push(r);

        if (edges[lastIndex] == 2)
            r[lastIndex] = 2;
        else
            r[lastIndex] = 0;

        for (let i = 0; i < lastIndex; i++) {
            if (edges[i] == 1)
                r[i] = 1;
            else
                r[i] = 0;
        }

        const weightMatrix = this.weightMatrix;
        const wr = new Array(lastIndex + 1);
        weightMatrix.push(wr);

        for (let i = 0; i < weightMatrix.length; i++) {
            const r = weightMatrix[i];
            if (i == lastIndex) {
                for (let j = 0; j < weightMatrix.length; j++) {
                    let w = weights[j];
                    if (!adjMatrix[i][j])
                        w = null;
                    else if (Array.isArray(w) && Number.isFinite(w[0]))
                        w = w[0];

                    wr[j] = w;
                }
            } else {
                let w = weights[i];
                if (!adjMatrix[i][lastIndex])
                    w = null;
                else if (Array.isArray(w) && Number.isFinite(w[1]))
                    w = w[1];

                r.push(w);
            }
        }
    }

    isDirected() {
        const matrix = this.adjMatrix;
        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < i; j++) {
                if (matrix[i][j] != matrix[j][i])
                    return true;
            }
        }

        return false;
    }

    isWeighted() {
        const matrix = this.weightMatrix;
        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < matrix.length; j++) {
                if (Math.abs(matrix[i][j]) > 1)
                    return true;
            }
        }

        return false;
    }

    clearWeights() {
        const size = this.adjMatrix.length, weights = [];
        for (let i = 0; i < size; i++) {
            weights.push(new Array(size));
            for (let j = 0; j < size; j++)
                weights[i][j] = 1;
        }

        return weightScope.set(this, weights);
    }

    getWeight(i, j) {
        return this.weightMatrix[i][j] || null;
    }

    setWeight(i, j, weight) {
        const size = this.weightMatrix.length;
        if (!Number.isInteger(i) || i < 0 || i >= size)
            throw new RangeError("Invalid position");

        if (!Number.isInteger(j) || j < 0 || j >= size)
            throw new RangeError("Invalid position");

        if (!Number.isFinite(weight))
            throw new TypeError("Invalid weight");

        this.weightMatrix[i][j] = weight;
    }

    connected(i, j) {
        return !!this.adjMatrix[i][j];
    }
}

function isValidAdjacencyMatrix(matrix) {
    if (!Array.isArray(matrix))
        return false;

    if (matrix.length == 0)
        return true;

    const rows = matrix.length, columns = matrix[0].length;
    if (rows != columns)
        return false;

    return matrix.every((r, i) => {
        if (!Array.isArray(r))
            return false;

        if (r.length != columns)
            return false;

        return r.every((n, j) => {
            if (i == j)
                return (n === 0 || n === 2);

            return (n === 0 || n === 1);
        });
    })
}

function isValidWeightMatrix(matrix, size) {
    if (!Array.isArray(matrix))
        return false;

    if (matrix.length != size)
        return false;

    return matrix.every(r => Array.isArray(r) && r.length == size && r.every(Number.isFinite));
}