export default function CustomEventTarget(target = new EventTarget()) {
    this._target = target;
    this._bound = new WeakMap();
}

Object.defineProperties(CustomEventTarget.prototype, {
    _target: {
        configurable: false,
        writable: true,
        enumerable: false,
        value: null
    },

    _bound: {
        configurable: false,
        writable: true,
        enumerable: false,
        value: null
    }
});

CustomEventTarget.appendTo = function appendTo(obj, ...args) {
    this.call(obj, ...args);
    while (Reflect.getPrototypeOf(obj) != Object.prototype)
        obj = Reflect.getPrototypeOf(obj);

    Reflect.setPrototypeOf(obj, this.prototype);
};

CustomEventTarget.prototype.addEventListener = function addEventListener(type, listener) {
    if (!Object.prototype.isPrototypeOf(listener))
        this._target.addEventListener(...arguments);

    if (typeof listener == 'function') {
        let obj = this._bound.get(listener);
        if (obj == null)
            this._bound.set(listener, obj = {});

        obj[type] = (_event) => {
            const { args, event } = _event.detail;
            if (args != null)
                listener.call(this, event, ...args);
            else
                listener.call(this, _event);
        };

        this._target.addEventListener(type, obj[type]);
    }
};

CustomEventTarget.prototype.removeEventListener = function removeEventListener(type, listener) {
    if (typeof listener != 'object')
        this._target.removeEventListener(type, listener);

    if (typeof listener == 'function') {
        let obj = this._bound.get(listener);
        if (obj != null && typeof obj[type] != 'undefined')
            this._target.removeEventListener(type, obj[type]);
    }
};

CustomEventTarget.prototype.dispatchEvent = function dispatchEvent(event, ...args) {
    return this._target.dispatchEvent(new CustomEvent(event.type, {
        detail: { args, event }
    }));
};

CustomEventTarget.wrapMethod = function wrapMethod(target, method, callbackfn) {
    if (typeof callbackfn != 'function' || typeof target[method] != 'function')
        throw new TypeError(`Failed to wrap CustomEventTarget ${method}`);

    const fn = target[method];
    target[method] = function(..._args) {
        let { returnValue, thisArg = this, args = _args } = callbackfn.apply(this, _args) || {};
        if (typeof returnValue != 'undefined')
            return returnValue;

        if (!Array.isArray(args))
            args = [args];

        return fn.apply(thisArg, args);
    };
};

CustomEventTarget.extend = function extend(...events) {
    const handlers = new WeakMap();
    function ProxyEventTarget() {
        CustomEventTarget.call(this);
        handlers.set(this, {});
    }

    Object.assign(ProxyEventTarget, CustomEventTarget);
    Reflect.setPrototypeOf(ProxyEventTarget.prototype, CustomEventTarget.prototype);

    ProxyEventTarget.prototype.dispatchEvent = function dispatchEvent(event, ...args) {
        const handler = this[`on${event.type}`];
        if (handler != null)
            handler.call(this, event, ...args);

        return CustomEventTarget.prototype.dispatchEvent.call(this, event, ...args);
    };

    if (events[0] instanceof Array)
        events = events[0];

    for (const ev of events) {
        Object.defineProperty(ProxyEventTarget.prototype, `on${ev}`, {
            get: function() {
                return handlers.get(this)[ev] || null;
            },

            set: function(handler) {
                if (typeof handler == 'function')
                    handlers.get(this)[ev] = handler;
            }
        });
    }

    return ProxyEventTarget;
};

CustomEventTarget.from = function from(target) {
    function BoundEventTarget() {
        CustomEventTarget.call(this, target);
    }

    Object.assign(BoundEventTarget, CustomEventTarget);
    Reflect.setPrototypeOf(BoundEventTarget.prototype, CustomEventTarget.prototype);
    return BoundEventTarget;
};