import { Path, Component, Canvas } from './canvas.js';
import CustomEventTarget from './custom-target.js';
import '../css/style.css';

(async function(global) {
    Object.assign(global, { Path, Component, Canvas, CustomEventTarget });

    const PI = Math.PI;
    const canvas = global.canvas = new Canvas(500,500);

    class Draggable extends Path {
        static state = Symbol('state');
        static initialX = Symbol('initialX');
        static initialY = Symbol('initialY');
        static currentX = Symbol('currentX');
        static currentY = Symbol('currentY');

        static getInitialScope(state, x, y) {
            return {
                [Draggable.state]: state,
                [Draggable.initialX]: x,
                [Draggable.initialY]: y,
                [Draggable.currentX]: x,
                [Draggable.currentY]: y
            };
        }

        constructor(renderer, scope, hooks) {
            if (typeof renderer != "function")
                throw new TypeError("renderer must be a function");

            const props = Object.assign(Draggable.getInitialScope(false, 0, 0), scope);
            super((path, ctx, scope) => {
                const result = renderer.call(scope, path, ctx, scope) || path;
                if (this.state) {
                    const { initialX, initialY, currentX, currentY } = this;
                    console.log('juego', currentX, initialX, currentY, initialY);
                    ctx.translate(currentX - initialX, currentY - initialY);
                    ctx.globalAlpha = 0.5;
                    ctx.stroke(result);

                    ctx.setTransform(1, 0, 0, 1, 0, 0);
                    ctx.globalAlpha = 1;
                }

                return result;
            }, props, hooks);

            this.addEventListener('added', function handler(event, container) {
                container.getCanvasElement().addEventListener('mousemove', (event) => {
                    if (this.get(Draggable.state)) {
                        this.update({
                            [Draggable.currentX]: event.offsetX,
                            [Draggable.currentY]: event.offsetY
                        });
                    }
                });

                this.removeEventListener('added', handler);
            });

            this.addEventListener('mousedown', (event) => {
                console.log(event);
                this.set({
                    [Draggable.state]: true,
                    [Draggable.initialX]: event.offsetX,
                    [Draggable.initialY]: event.offsetY
                });
            });

            global.addEventListener('mouseup', () => this.update(Draggable.state, false));
        }

        get state() {
            return this._scope[Draggable.state];
        }

        get initialX() {
            return this._scope[Draggable.initialX];
        }

        get initialY() {
            return this._scope[Draggable.initialY];
        }

        get currentX() {
            return this._scope[Draggable.currentX];
        }

        get currentY() {
            return this._scope[Draggable.currentY];
        }
    }

    const insertGraph = global.d1 = new Draggable((path, ctx) => {
        path.arc(50, 25, 15, 0, 2 * PI);
        ctx.strokeText("1", 50, 25);
    });

    canvas.add(insertGraph);
    canvas.add((ctx) => {
        ctx.moveTo(0, 50);
        ctx.lineTo(500, 50);
    });

    canvas.render((ctx) => {
        if (!global.ctx)
            global.ctx = ctx;

        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
    });
})(typeof window != 'undefined' ? window : null);