import CustomEventTarget from "./custom-target";

function getOwnProperties(object) {
    object = object || {};
    return [
        ...Object.keys(object),
        ...Object.getOwnPropertySymbols(object)
    ];
}

const valuesMap = new WeakMap();
export class Component {
    _scope = {};
    _hooks = {};
    container = null;
    render = null;

    constructor(renderer, scope, hooks) {
        if (renderer == null)
            return;

        if (typeof renderer != "function")
            throw new TypeError("renderer must be a function");

        this.render = renderer;
        const values = {};

        for (const key of getOwnProperties(scope)) {
            values[key] = scope[key];
            Object.defineProperty(this._scope, key, {
                configurable: true,
                enumerable: true,

                get: () => values[key],
                set: (newValue) => {
                    values[key] = newValue;
                    this.container.update();
                }
            });
        }

        Object.assign(this._hooks, hooks),
        valuesMap.set(this, values);
        Object.seal(this._scope);
        Object.seal(values);
    }

    get(key) {
        return this._scope[key] || null;
    }

    set(...args) {
        const values = valuesMap.get(this);
        if (Object.prototype.isPrototypeOf(args[0])) {
            Object.assign(values, args[0]);
            return;
        }

        const [key, value] = args;
        if (!this._scope.hasOwnProperty(key))
            return;

        values[key] = value;
    }

    setWith(...args) {
        const values = valuesMap.get(this);
        if (typeof args[0] == 'function') {
            args[0](values);
            return;
        }

        const [key, callbackfn] = args;
        if (!this._scope.hasOwnProperty(key))
            return;

        let value = this._scope[key];
        value = callbackfn(value);
        values[key] = value;
    }

    update(...args) {
        this.set(...args);
        if (this.container != null)
            this.container.update();
    }

    updateWith(...args) {
        this.setWith(...args);
        if (this.container != null)
            this.container.update();
    }
}

const PathEvents = 'added removed mousemove mousedown mouseup mousewheel click dblclick contextmenu mouseover mouseout'.split(' ');
const PathEventTarget = CustomEventTarget.extend(PathEvents);

export class Path extends Component {
    last = null;
    stroke = null;

    constructor(renderer, scope, hooks) {
        super(renderer, scope, hooks);
        PathEventTarget.appendTo(this);
    }
}

CustomEventTarget.wrapMethod(PathEventTarget.prototype, 'dispatchEvent', function(event, ...args) {
    if (args.length > 0)
        return { args: Array.from(arguments) };

    const params = { args: [event] };
    if (event.type != 'mousemove') {
        const container = this.container, path = new Path2D();
        params.args.push(path, container.getPathRenderer(this, path), container._ctx);
    }

    return params;
});

const callbacks = new WeakMap();
export class Canvas {
    _components = new Set();
    _state = new WeakMap();
    _elem = document.createElement("canvas");
    _ctx = this._elem.getContext('2d');

    constructor(width = 0, height = 0) {
        this.width = width;
        this.height = height;
    }

    get width() {
        return this._elem.width;
    }

    set width(w) {
        this._elem.width = w;
    }

    get height() {
        return this._elem.height;
    }

    set height(h) {
        this._elem.height = h;
    }

    render(callback, context) {
        if (context != null && !Element.prototype.isPrototypeOf(context))
            throw new TypeError(`Invalid context, ${context} is not an element`);

        let lastEvent = null;
        const tracker = Object.create({ x: null, y: null }, {
            event: {
                get: () => lastEvent,
                set: (event) => {
                    const type = (event.type in Canvas.Handlers) ? event.type : 'default';
                    lastEvent = event;

                    for (const p of this._components) {
                        if (!Path.prototype.isPrototypeOf(p) || p.last == null)
                            continue;

                        Canvas.Handlers[type].call(this, p, event);
                    }
                }
            }
        });

        window.addEventListener('focus', () => {
            if (lastEvent == null)
                return;

            for (const p of this._components) {
                if (!Path.prototype.isPrototypeOf(p) || p.last == null)
                    continue;

                Canvas.Handlers.mousemove.call(this, p, lastEvent);
            }
        });

        for (const e of PathEvents) {
            this._elem.addEventListener(e, (event) => {
                tracker.x = event.offsetX;
                tracker.y = event.offsetY;
                tracker.event = event;
            });
        }

        const render = () => {
            context = context || document.body;
            context.appendChild(this._elem);
            callback.call(this, this._ctx, this),
            callbacks.set(this, callback);
            this.update()
        };

        if (document.readyState == 'loading')
            document.addEventListener("DOMContentLoaded", render);
        else
            render();
    }

    add(...args) {
        let component = args[0];
        if (typeof component == 'function')
            component = new Component(...args);

        if (!Component.prototype.isPrototypeOf(component))
            throw new TypeError(`${component} is not a component`);

        component.container = this;
        this._components.add(component);

        if (component instanceof Path)
            component.dispatchEvent(new CustomEvent('added'), this);

        return this;
    }

    addPath(...args) {
        this.add(new Path(...args));
    }

    remove(component) {
        if (this._components.delete(component)) {
            component.dispatchEvent(new CustomEvent('removed'), this);
            this.update();
            return true;
        }

        return false;
    }

    update(...components) {
        const elem = this._elem, ctx = this._ctx;
        const callback = callbacks.get(this);
        const renderAll = !Boolean(components.length);

        if (renderAll)
            ctx.clearRect(0, 0, elem.width, elem.height);

        if (callback != null)
            callback.call(this, this._ctx, this);

        if (renderAll)
            components = this._components;

        for (const component of components) {
            const { render, _scope, _hooks } = component;
            let stroke = (component instanceof Path) ? component.stroke : null;

            if (stroke == null) {
                for (const key in _hooks) {
                    if (key in _scope)
                        component.set(key, _hooks[key](_scope[key]));
                }
            }

            if (component instanceof Path) {
                if (stroke == null) {
                    stroke = new Path2D();
                    stroke = render.call(_scope, stroke, ctx, _scope) || stroke;
                }

                if (stroke instanceof Path2D) {
                    ctx.stroke(stroke);
                    component.last = stroke;
                }

                component.stroke = null;
                continue;
            }

            ctx.beginPath();
            render.call(_scope, ctx, _scope);
            ctx.stroke();
            ctx.closePath();
        }
    }

    getPathRenderer(path, stroke) {
        return (path2d = stroke) => {
            path.stroke = path2d;
            this.update();
        };
    }

    clear() {
        this._components.clear();
        this.update();
    }

    getCanvasElement() {
        return this._elem;
    }
}

Canvas.Handlers = {
    mouseout: function(path, event) {
        this._state.set(path, false);
        path.dispatchEvent(new MouseEvent('mouseout', event));
    },

    mousemove: function(path, event) {
        const ev = new MouseEvent('mouseover', event);
        console.log('a)', event.offsetX, event.offsetY);
        console.log('b)', ev.offsetX, ev.offsetY);

        const state = this._state.get(path);
        if (this._ctx.isPointInPath(path.last, event.offsetX, event.offsetY)) {
            if (!state) {
                this._state.set(path, true);
                path.dispatchEvent(new MouseEvent('mouseover', event));
            }

            path.dispatchEvent(new MouseEvent('mousemove', event));
        } else if (state) {
            this._state.set(path, false);
            path.dispatchEvent(new MouseEvent('mouseout', event));
        }
    },

    default: function(path, event) {
        if (this._ctx.isPointInPath(path.last, event.offsetX, event.offsetY))
            path.dispatchEvent(new MouseEvent(event.type, event));
    }
};